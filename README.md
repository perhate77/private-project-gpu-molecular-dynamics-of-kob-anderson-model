**There are two parallel GPU Molecular dynamics code of Kob Anderson (KA) model with
 DPD thermostat and Nose Hover thermostat, written in CUDA in this directory.**

   
 * I have added a comment with capital letters "TWEAK THIS" ahead of those  	 variables which you can tweak and see the results
 	 
 * You must have the nvcc compiler installed.

 * To install nvcc compiler in linux, first install recommended nvidia driver and then install cuda with the folloing command
    

 *      sudo apt install nvidia-cuda-toolkit

 *	 Compile KA DPD code using: 
 *      nvcc md_gpu_KobAndersonDPD.cu -arch=sm_61 -lm -Xptxas -O3,-v --use_fast_math -w

 *	 Compile KA Nose Hover code using: 
 *	 	 nvcc -arch=sm_61 -rdc=true -lcudadevrt md_gpu_KobAndersonNoseHover_DynamicParallelism.cu -w -lm  -Xptxas -O3,-v   --use_fast_math -w

-rdc=true  -lcudadevrt is used for dynamic parallelism
     Note that dynamic parallelism is only supported in V100 and later GPU
     cards. 
      
      -arch=sm_61 is the flag used for the GPU card architecture. 
      -arch=sm_61    is used for Geforce GTX 1050 Ti (my machine’s GPU). To run above codes on
     Tesla V100, change -arch=sm_61 to -arch=sm_70.



 *	 Run :
 *		 nohup ./a.out &
 	 			


  * Read input Data:<br/>
  <pre>
  Coordinate file   :: 		positionLAMMPS.dat (N=4000, number density = 1.2)
  Format:

    Particle Id		type	x	y	z	mx	my	mz
    ....

  Velocity file   :: 		velocityLAMMPS.dat
  Format:

     Particle Id		vx	vy	vz
     ....</pre>

**Benchmarking with LAMMPS (Average potential energy)**


| T | LAMMPS Results, (\<PE\>) | CUDA Code Results, (\<PE\>)  |C Code Results, (\<PE\>) |
| ------ | ------ | ------ | ------ |
| 0.6 | -6.03647 | -6.03470  | -6.03508 |
| 1.0 | -5.34593 |  -5.34219 | -5.34503 |

Average potential energy obtained from LAMMPS and our
CUDA code for Kob-Anderson model at two temperature, T = 0.6, 1.0 and density, ρ = 1.2.
We have equilibrated the system for 10 6 MD steps and 1000 samples after every 10 4 MD steps
are generated for averaging.
     
![DPD_CPUGPUTimePY](/uploads/d412a7b7023369b0ef55cb82b8d91a6f/DPD_CPUGPUTimePY.png)
CPU vs. GPU time as a function of system size to complete 10000 MD steps for
the Kob Anderson model with DPD thermostat at temperature, T = 1.0 and number density,
ρ = 1.2.



 Written By::<br/>
       **Rajneesh Kumar**
 *	Theoretical Sciences Unit, Jawaharlal Nehru Centre for Advanced Scientific Research,
 *	Bengaluru 560064, India.
 *	Email: rajneesh[at]jncasr.ac.in
 *	27 Dec, 2020

